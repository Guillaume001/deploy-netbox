# Deploy NetBox via Puppet Bolt using Vagrant

## Requirement

* vagrant
* vagrant-libvirt
* bolt

```
git clone https://gitlab.com/Guillaume001/deploy-netbox.git
```

```
vagrant up
```

```
bolt plan run profile::netbox -t netbox_target
```

```
vagrant ssh netbox
# Create your own super user
sudo -u netbox bash -c "source /opt/netbox/venv/bin/activate && cd /opt/netbox/netbox && python3 manage.py createsuperuser"
```

[Now connect to your NetBox (http://192.168.20.100)](http://192.168.20.100)