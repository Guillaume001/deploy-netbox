plan profile::netbox (
  TargetSpec $targets
) {
  apply_prep([$targets])

  apply($targets) {
    $netbox_secret_key = lookup('netbox_secret_key')
    class { ::profile::netbox:
      netbox_secret_key => $netbox_secret_key,
    }
  }
}
