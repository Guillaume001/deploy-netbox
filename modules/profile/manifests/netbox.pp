# Netbox
#
# Profile for running Netbox through the ahs/netbox-module
#
class profile::netbox (
  String $netbox_secret_key,
) {

  selinux::boolean { 'httpd_can_network_connect': }
  # usermod apache -G netbox

  # Package may not installed by default
  # Necessary to get the locate 'en_US.UTF-8'
  package { 'langpack-en':
    name   => 'glibc-langpack-en',
    ensure => present,
  }

  class { 'netbox':
    secret_key    => $netbox_secret_key,
    allowed_hosts => [$trusted[certname]],
    require       => Package['langpack-en'],
  }
  
  class { 'apache':
    default_vhost => false,
  }

  class { 'apache::mod::wsgi':
    mod_path     => '/usr/lib64/httpd/modules/mod_wsgi_python3.so',
    package_name => 'python3.11-mod_wsgi',
  }

  apache::vhost { $trusted[certname]:
    use_port_for_filenames  => true,
    servername              => $trusted[certname],
    port                    => 80,
    proxy_preserve_host     => true,
    docroot                 => '/opt/netbox/netbox/',
    request_headers         => [
      'set "X-Forwarded-Proto" expr=%{REQUEST_SCHEME}',
    ],
    proxy_pass              => [
      {
        path        => '/',
        url         => 'http://127.0.0.1:8001/',
        reverse_url => 'http://127.0.0.1:8001/'
      },
    ],
    aliases                 => [
      {
        alias => '/static',
        path  => '/opt/netbox/netbox/static',
      },
    ],
    wsgi_pass_authorization => 'On',
    directories             => [
    {
      path            => '/opt/netbox/netbox/static',
      provider        => 'directory',
      custom_fragment => 'Options Indexes FollowSymLinks MultiViews'
    },
    {
      path            => '/static',
      provider        => 'location',
      custom_fragment => 'ProxyPass !'
    },
  ],
  }
}
